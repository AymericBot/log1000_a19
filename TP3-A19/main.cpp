#include <iostream>
#include <vector>

#include "voiture.h"
#include "mecanicienne.h"
#include "moteur.h"
#include "moteur_putput.h"
#include "moteur_beepbeep.h"

int main() {

    std::vector<Voiture*> voitures;
    std::vector<Moteur*> moteurs;
    std::vector<Mecanicienne*> mecaniciennes;

    // Création des objets
    moteurs.push_back(new Putput(1123));
    moteurs.push_back(new Beepbeep(2001, 6));
    moteurs.push_back(new Beepbeep(2558, 8));

	voitures.push_back(new Voiture('A'));
	voitures.push_back(new Voiture('B'));
	voitures.push_back(new Voiture('C'));

    mecaniciennes.push_back(new Mecanicienne(100));
    mecaniciennes.push_back(new Mecanicienne(161));
    mecaniciennes.push_back(new Mecanicienne(553));

    mecaniciennes[0]->assigneeSur(voitures[0]);
    mecaniciennes[0]->assigneeSur(voitures[1]);
    mecaniciennes[1]->assigneeSur(voitures[1]);
    mecaniciennes[2]->assigneeSur(voitures[0]);     
    mecaniciennes[2]->assigneeSur(voitures[1]);
    mecaniciennes[2]->assigneeSur(voitures[2]);

    voitures[0]->ajouterMoteur(moteurs[0]);
    voitures[1]->ajouterMoteur(moteurs[2]);
    voitures[2]->ajouterMoteur(moteurs[1]);
    voitures[2]->enleverMoteur();

    mecaniciennes[2]->aTermineAvec(voitures[0]);

    // Affichage des objets
    std::cout << "Voitures : " << std::endl;
    for(int v=0 ; v<voitures.size() ; v++)
    {
        std::cout << "  Voiture " << voitures[v]->getID() << " : ";
        if (voitures[v]->getMoteur()==NULL)
            std::cout << "pas de moteur";
        else {
            std::cout << "Moteur #" << voitures[v]->getMoteur()->getNumeroSerie();
            std::cout << " d'une puissance de ";
            std::cout << voitures[v]->getMoteur()->getPuissance() << " cv";
        }
        std::cout << std::endl;
    }

    std::cout << "Moteurs : " << std::endl;
    for(int m=0 ; m<moteurs.size() ; m++) {
        std::cout << "  Moteur #" << moteurs[m]->getNumeroSerie() << " : ";
        std::cout << moteurs[m]->getNbPistons() << " pistons et d'une";
        std::cout << " puissance de ";
        std::cout << moteurs[m]->getPuissance() << " cv";
        std::cout << std::endl;
    }   

    std::cout << "Mécaniciennes : " << std::endl;
    for (int m=0 ; m<mecaniciennes.size() ; m++) {
        std::cout << "  Mécanicienne #" << mecaniciennes[m]->getMatricule();
        std::cout << " a les assignations suivantes : " << std::endl;
        mecaniciennes[m]->afficheAssignations();
    }

    // Effacement des objets
    for(int v=0 ; v<voitures.size() ; v++) {
        Voiture* une_voiture = voitures.back();
        voitures.pop_back();
        delete une_voiture;
    }

    for(int m=0 ; m<moteurs.size() ; m++) {
        Moteur* un_moteur = moteurs.back();
        moteurs.pop_back();
        delete un_moteur;
    }

    for(int m=0 ; m<mecaniciennes.size() ; m++) {
        Mecanicienne* une_mecanicienne = mecaniciennes.back();
        mecaniciennes.pop_back();
        delete une_mecanicienne;
    }

	return 0;
}
